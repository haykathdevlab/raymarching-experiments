﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RaymarchingBoundingBox : MonoBehaviour
{

    const string BoundingBoxMinPropertyName = "_BoundingBoxMin";
    const string BoundingBoxMaxPropertyName = "_BoundingBoxMax";

    Vector4 BBMin;
    Vector4 BBMax;

    Renderer r;

    // Use this for initialization
    void Start()
    {
        BBMin = new Vector4(-1, -1, -1, 1);
        BBMax = new Vector4(1, 1, 1, 1);

        r = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBoundingBox();
        SetShaderProperties();
    }

    void UpdateBoundingBox()
    {
        BBMin.x = r.bounds.min.x;
        BBMin.y = r.bounds.min.y;
        BBMin.z = r.bounds.min.z;

        BBMax.x = r.bounds.max.x;
        BBMax.y = r.bounds.max.y;
        BBMax.z = r.bounds.max.z;
    }

    void SetShaderProperties()
    {
        if (Application.isPlaying)
        {
            r.material.SetVector(BoundingBoxMinPropertyName, BBMin);
            r.material.SetVector(BoundingBoxMaxPropertyName, BBMax);
        }
        else
        {
            r.sharedMaterial.SetVector(BoundingBoxMinPropertyName, BBMin);
            r.sharedMaterial.SetVector(BoundingBoxMaxPropertyName, BBMax);
        }
    }
}

struct Ray {
	float3 origin;
	float3 direction;
};

struct Bounds {
	float3 Min;
	float3 Max;
};

struct Plane {
	float3 pos;
	float3 norm;
};

float4 GetColor(uniform sampler3D volume, float3 pos){
	float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
	float3 objScale = 1/recipObjScale;

	float3 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
	
	
	pos -= objPos;
	pos *= recipObjScale;
	
	return tex3D(volume, float4(pos + .5, 0)); 
	//return float4(pos, 1);
}

float GetCut(float3 pos, Plane p){
	float3 dist = normalize(pos - p.pos);
	float3 norm = normalize(p.norm);

	return ceil(saturate(-dot(dist, norm)));
}

//BBIntersect
bool IntersectBox(Ray r, float3 boxmin, float3 boxmax, out float tnear, out float tfar)
{	
	// compute intersection of ray with all six bounding box planes
	float3 invR = 1.0 / r.direction;
	float3 tbot = invR * (boxmin.xyz - r.origin);
	float3 ttop = invR * (boxmax.xyz - r.origin);
	// re-order intersections to find smallest and largest on each axis
	float3 tmin = min (ttop, tbot);
	float3 tmax = max (ttop, tbot);
	// find the largest tmin and the smallest tmax
	float2 t0 = max (tmin.xx, tmin.yz);
	tnear = max (t0.x, t0.y);
	t0 = min (tmax.xx, tmax.yz);
	tfar = min (t0.x, t0.y);
	// check for hit
	bool hit;
	if ((tnear > tfar))
		hit = false;
	else
		hit = true;
	return hit;
}


//new Raymarch
float4 RayMarchPS(Ray eyeray , uniform int steps, uniform Bounds bounds, uniform Plane cuttingPlane, uniform sampler3D volume) : COLOR
{
	eyeray.direction = normalize(eyeray.direction);

	// calculate ray intersection with bounding box
	float tnear, tfar;
	bool hit = IntersectBox(eyeray, bounds.Min, bounds.Max, tnear, tfar);
	if (!hit) discard;
	if (tnear < 0.0) tnear = 0.0;

	// calculate intersection points
	float3 Pnear = eyeray.origin + eyeray.direction*tnear;
	float3 Pfar = eyeray.origin + eyeray.direction*tfar;

	// march along ray, accumulating color
	float4 dest = 0;
	half3 rayStep = (Pnear - Pfar) / (steps-1);
	half3 P = Pfar;

	for(int i=0; i<steps; i++) {

		float4 src = saturate(GetColor(volume, P));
		src.a *= GetCut(P, cuttingPlane);

		float4 o = 0;
		//Blending
		o.rgb = src.a*src.rgb + (1-src.a) * dest.rgb;
		o.a = src.a + dest.a*(1 - src.a);

		dest = o;
		P += rayStep;
	}

	//dest /= steps;
	return dest;
}

//MRI Version
float4 RayMarchMRI(Ray eyeray , uniform int steps, uniform Bounds bounds, uniform Plane cuttingPlane, uniform sampler3D volume) : COLOR
{
	eyeray.direction = normalize(eyeray.direction);

	// calculate ray intersection with bounding box
	float tnear, tfar;
	bool hit = IntersectBox(eyeray, bounds.Min, bounds.Max, tnear, tfar);
	if (!hit) discard;
	if (tnear < 0.0) tnear = 0.0;

	// calculate intersection points
	float3 Pnear = eyeray.origin + eyeray.direction*tnear;
	float3 Pfar = eyeray.origin + eyeray.direction*tfar;

	// march along ray, accumulating color
	float4 dest = 0;
	half3 rayStep = (Pnear - Pfar) / (steps-1);
	half3 P = Pfar;

	for(int i=0; i<steps; i++) {

		float4 src = saturate(GetColor(volume, P));
		src.a *= GetCut(P, cuttingPlane);

		if(length(src.rgb) < .12)
			src.a = 0;
		else
			src.a *= length(src.rgb);
		

		float4 o = 0;
		//Blending
		o.rgb = src.a*src.rgb + (1-src.a) * dest.rgb;
		o.a = src.a + dest.a*(1 - src.a);

		dest = o;
		P += rayStep;
	}

	//dest /= steps;
	return dest;
}


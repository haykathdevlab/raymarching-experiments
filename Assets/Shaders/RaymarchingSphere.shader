// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33880,y:32557,varname:node_9361,prsc:2|custl-6557-OUT,clip-7010-OUT;n:type:ShaderForge.SFN_Code,id:7801,x:32747,y:32800,varname:node_7801,prsc:2,code:ZgBsAG8AYQB0ADMAIABzAGEAbQBwAGwAZQBQAG8AcwAgAD0AIABmAGwAbwBhAHQAMwAoADAALAAwACwAMAApADsACgBmAGwAbwBhAHQAMwAgAHMAYQBtAHAAbABlAE8AZgBmAHMAZQB0ACAAPQAgAFYAaQBlAHcARABpAHIAIAAqACAAUwB0AGUAcABTAGkAegBlADsACgBmAGwAbwBhAHQANAAgAGMAbwBsAG8AcgAgAD0AIABmAGwAbwBhAHQANAAoADAALAAwACwAMAAsADAAKQA7AAoACgBpAGYAKABkAGkAcwB0AGEAbgBjAGUAKABXAG8AcgBsAGQAUABvAHMALAAgAEMAYQBtAFAAbwBzACkAIAA8ACAAQwBsAGkAcABwAGkAbgBnAFAAbABhAG4AZQBQAG8AcwApAHsACgAgACAAIAAgAHMAYQBtAHAAbABlAFAAbwBzACAAPQAgAEMAYQBtAFAAbwBzACAAKwAgAFYAaQBlAHcARABpAHIAIAAqACAAQwBsAGkAcABwAGkAbgBnAFAAbABhAG4AZQBQAG8AcwA7AAoAfQAKAGUAbABzAGUAewAKACAAIAAgACAAcwBhAG0AcABsAGUAUABvAHMAIAA9ACAAVwBvAHIAbABkAFAAbwBzADsACgB9AAoACgBmAG8AcgAoAGkAbgB0ACAAaQAgAD0AIAAwADsAIABpACAAPAAgAFMAdABlAHAATQBhAHgAOwAgAGkAKwArACkAewAKACAAIAAgACAAaQBmACgAZABpAHMAdABhAG4AYwBlACgAcwBhAG0AcABsAGUAUABvAHMALAAgAFMAcABoAGUAcgBlAFAAbwBzACkAIAA8AD0AIABTAHAAaABlAHIAZQBSAGEAZABpAHUAcwApAHsACgAgACAAIAAgACAAIAAgACAAYwBvAGwAbwByACAAPQAgAGYAbABvAGEAdAA0ACgAcwBhAG0AcABsAGUAUABvAHMALAAgADEALgAwACkAOwAKACAAIAAgACAAIAAgACAAIABiAHIAZQBhAGsAOwAKACAAIAAgACAAfQAKACAAIAAgACAAcwBhAG0AcABsAGUAUABvAHMAIAArAD0AIABzAGEAbQBwAGwAZQBPAGYAZgBzAGUAdAA7AAoAfQAKAAoAcgBlAHQAdQByAG4AIABjAG8AbABvAHIAOwA=,output:3,fname:Raymarching,width:628,height:263,input:2,input:2,input:0,input:0,input:0,input:2,input:0,input:2,input_1_label:CamPos,input_2_label:ViewDir,input_3_label:ClippingPlanePos,input_4_label:StepSize,input_5_label:StepMax,input_6_label:SpherePos,input_7_label:SphereRadius,input_8_label:WorldPos|A-9040-XYZ,B-4170-OUT,C-9229-NEAR,D-7177-OUT,E-1464-OUT,F-659-XYZ,G-2107-OUT,H-3859-XYZ;n:type:ShaderForge.SFN_ViewPosition,id:9040,x:32233,y:32687,varname:node_9040,prsc:2;n:type:ShaderForge.SFN_ViewVector,id:2980,x:32233,y:32820,varname:node_2980,prsc:2;n:type:ShaderForge.SFN_ProjectionParameters,id:9229,x:32233,y:32968,varname:node_9229,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:7177,x:32233,y:33127,ptovrint:False,ptlb:StepSize,ptin:_StepSize,varname:node_7177,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_ValueProperty,id:1464,x:32233,y:33220,ptovrint:False,ptlb:StepMax,ptin:_StepMax,varname:node_1464,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:200;n:type:ShaderForge.SFN_ComponentMask,id:6557,x:33468,y:32803,varname:node_6557,prsc:2,cc1:0,cc2:1,cc3:2,cc4:-1|IN-7801-OUT;n:type:ShaderForge.SFN_ComponentMask,id:7010,x:33468,y:32962,varname:node_7010,prsc:2,cc1:3,cc2:-1,cc3:-1,cc4:-1|IN-7801-OUT;n:type:ShaderForge.SFN_Negate,id:4170,x:32406,y:32820,varname:node_4170,prsc:2|IN-2980-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2107,x:32233,y:33488,ptovrint:False,ptlb:Radius,ptin:_Radius,varname:node_2107,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ObjectPosition,id:659,x:32233,y:33303,varname:node_659,prsc:2;n:type:ShaderForge.SFN_FragmentPosition,id:3859,x:32233,y:33551,varname:node_3859,prsc:2;proporder:7177-1464-2107;pass:END;sub:END;*/

Shader "Shader Forge/RaymarchingSphere" {
    Properties {
        _StepSize ("StepSize", Float ) = 0.1
        _StepMax ("StepMax", Float ) = 200
        _Radius ("Radius", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0

            float4 Raymarching( float3 CamPos , float3 ViewDir , float ClippingPlanePos , float StepSize , float StepMax , float3 SpherePos , float SphereRadius , float3 WorldPos ){
				float3 samplePos = float3(0,0,0);
				float3 sampleOffset = ViewDir * StepSize;
				float4 color = float4(0,0,0,0);
            
				if(distance(WorldPos, CamPos) < ClippingPlanePos){
					samplePos = CamPos + ViewDir * ClippingPlanePos;
				}
				else{
					samplePos = WorldPos;
				}
            
				for(int i = 0; i < StepMax; i++){
					if(distance(samplePos, SpherePos) <= SphereRadius){
						color = float4(samplePos, 1.0);
						break;
					}
					samplePos += sampleOffset;
				}
            
				return color;
            }
            
            uniform float _StepSize;
            uniform float _StepMax;
            uniform float _Radius;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_7801 = Raymarching( _WorldSpaceCameraPos , (-1*viewDirection) , _ProjectionParams.g , _StepSize , _StepMax , objPos.rgb , _Radius , i.posWorld.rgb );
                clip(node_7801.a - 0.5);
////// Lighting:
                float3 finalColor = node_7801.rgb;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            float4 Raymarching( float3 CamPos , float3 ViewDir , float ClippingPlanePos , float StepSize , float StepMax , float3 SpherePos , float SphereRadius , float3 WorldPos ){
            float3 samplePos = float3(0,0,0);
            float3 sampleOffset = ViewDir * StepSize;
            float4 color = float4(0,0,0,0);
            
            if(distance(WorldPos, CamPos) < ClippingPlanePos){
                samplePos = CamPos + ViewDir * ClippingPlanePos;
            }
            else{
                samplePos = WorldPos;
            }
            
            for(int i = 0; i < StepMax; i++){
                if(distance(samplePos, SpherePos) <= SphereRadius){
                    color = float4(samplePos, 1.0);
                    break;
                }
                samplePos += sampleOffset;
            }
            
            return color;
            }
            
            uniform float _StepSize;
            uniform float _StepMax;
            uniform float _Radius;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 posWorld : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_7801 = Raymarching( _WorldSpaceCameraPos , (-1*viewDirection) , _ProjectionParams.g , _StepSize , _StepMax , objPos.rgb , _Radius , i.posWorld.rgb );
                clip(node_7801.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

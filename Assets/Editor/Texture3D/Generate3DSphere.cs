﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Generate3DSphere : EditorWindow
{
    const string Title = "3D Sphere Texture Generator";
    const string fileExt = ".asset";

    private string path = "Assets/Tex3D/";
    private string assetName = "TextureName";
    
    private Vector3 dimensions = new Vector3(32, 32, 32);
    private float radius = .45f;
    private float alpha = .1f;

    [MenuItem("Texture3D/SphereGenerator")]
    public static void ShowWindow()
    {
        var window = GetWindow(typeof(Generate3DSphere));
        window.Show();
    }

    void OnEnable()
    {
        titleContent = new GUIContent(Title);
    }

    // Use this for initialization
    void OnGUI()
    {
        GUILayout.BeginVertical();
        EditorGUILayout.BeginVertical();

        GUILayout.Label("Texture Parameters");
        EditorGUILayout.Separator();

        //Texture params
        dimensions = EditorGUILayout.Vector3Field("Texture Dimensions", dimensions);
        radius = EditorGUILayout.FloatField("Sphere Radius", radius);
        alpha = EditorGUILayout.Slider("Alpha", alpha, 0, 1);
        EditorGUILayout.Separator();

        //File
        GUILayout.Label("Asset File");
        EditorGUILayout.Separator();

        path = EditorGUILayout.TextField("Path", path);
        assetName = EditorGUILayout.TextField("File Name", assetName);
        EditorGUILayout.Separator();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button("Save Asset", GUILayout.ExpandWidth(false)))
        {
            Texture3D tex = GenerateTexture();
            GenerateAsset(tex);
            AssetDatabase.Refresh();
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();
        GUILayout.EndVertical(); 
    }

    Texture3D GenerateTexture()
    {
        Texture3D tex = new Texture3D((int)dimensions.x, (int)dimensions.y, (int)dimensions.z, TextureFormat.ARGB32, false);
        int id = 0;
        Color[] colors = new Color[tex.width * tex.height * tex.depth];
        for (int i = 0; i < colors.Length; i++) colors[i].a = 0;

        Color cur = new Color(0,0,0,alpha);
        float stepx = 1f / tex.width;
        float stepy = 1f / tex.height;
        float stepz = 1f / tex.depth;
        Vector3 v = new Vector3();

        for (int i = 0; i < tex.depth; i++)
        {
            for (int j = 0; j < tex.width; j++)
            {
                for (int k = 0; k < tex.height; k++, id++)
                {
                    v.x = i * stepx;
                    v.y = j * stepy;
                    v.z = k * stepz;

                    cur.r = cur.g = cur.b = 0;
                    if (j < tex.height / 2) cur.r = 1;
                    else cur.b = 1;

                    if (Vector3.Distance(Vector3.one * 0.5f, v) <= radius)
                    {
                        var idx = j + (k * tex.width) + (i * (tex.width * tex.height));
                        colors[idx] = cur;
                    }
                }
            }
        }

        tex.wrapMode = TextureWrapMode.Clamp;
        tex.filterMode = FilterMode.Trilinear;
        tex.SetPixels(colors);
        tex.Apply();
        return tex;
    }

    void GenerateAsset(Texture3D tex)
    {
        AssetDatabase.CreateAsset(tex, path + assetName + fileExt);
    }
}
